import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AppService } from '../app.service';

@Component({
  selector: 'app-planos',
  templateUrl: './planos.component.html',
  styleUrls: ['./planos.component.sass']
})
export class PlanosComponent implements OnInit {

  public planos = [];
  public platId = '';

  constructor(private route: ActivatedRoute, private service: AppService) {
  }

  ngOnInit() {
    const app = this;
    this.route.params.subscribe(params => {

      app.showPlanos(params.id);
      app.platId = params.id;
    });
  }

  showPlanos(id) {
    const app = this;

    this.service.getPlanos(id)
      .subscribe((data) => {
        app.planos = (data as any).planos;
        app.service.planos = app.planos;
      });
  }

}
