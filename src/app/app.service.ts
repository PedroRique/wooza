import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class AppService {

  public plataformas;
  public planos;

  constructor(private http: HttpClient) { }

  getPlataformas(){
    let url = 'http://private-59658d-celulardireto2017.apiary-mock.com/plataformas';

    return this.http.get(url);
  }

  getPlanos(id){
    let url = 'http://private-59658d-celulardireto2017.apiary-mock.com/planos/' + id;

    return this.http.get(url);
  }
}
