import { Component, OnInit } from '@angular/core';
import { FormControl, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { AppService } from '../app.service';

declare var $: any;

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.sass']
})
export class FormComponent implements OnInit {

  profileForm = this.fb.group({
    nome: new FormControl('', Validators.required),
    email: new FormControl('', Validators.required),
    dataNasc: new FormControl('', Validators.required),
    cpf: new FormControl('', Validators.required),
    telefone: new FormControl('', Validators.required),
  });

  constructor(private fb: FormBuilder, private route: ActivatedRoute, private service: AppService) { }

  public platId;
  public planoId;

  ngOnInit() {
    const app = this;
    this.route.params.subscribe(params => {
      app.platId = params.platId;
      app.planoId = params.planoId;
    });

    if(!this.service.plataformas){ //tratativa caso chegue até essa tela sem as informações necessárias
      this.service.getPlataformas()
      .subscribe((data) => {
        let plataforma = (data as any).plataformas.filter((a,b,c) => {
          return a.sku == app.platId;
        });

        console.log('Plataforma selecionada:', plataforma);
      });
    }

    if(!this.service.planos){ //tratativa caso chegue até essa tela sem as informações necessárias
      this.service.getPlanos(this.platId)
      .subscribe((data) => {

        let plano = (data as any).planos.filter((a,b,c) => {
          return a.sku == app.planoId;
        });

        console.log('Plano selecionado:', plano);
      });
    }
  }

  ngAfterViewInit(){
    $('#cpf').mask('000.000.000-00', {reverse: true});

    var maskBehavior = function (val) {
      return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
    },
    options = {
      onKeyPress: function(val, e, field, opt) {
          field.mask(maskBehavior.apply({}, arguments), opt);
        }
    };
    
    $('#telefone').mask(maskBehavior, options);

    $('#dataNasc').mask('00/00/0000');
  }

  onSubmit(form){
    const app = this;
    console.log('Usuário:', form.value);
  }

}
