import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MainComponent } from './main/main.component';
import { PlanosComponent } from './planos/planos.component';
import { FormComponent } from './form/form.component';

const routes: Routes = [
  {
    path: '', component: MainComponent
  },
  {
    path: 'planos/:id', component: PlanosComponent
  },
  {
    path: 'final/:platId/:planoId', component: FormComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
