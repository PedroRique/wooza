import { Component, OnInit } from '@angular/core';
import { AppService } from '../app.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-plataformas',
  templateUrl: './plataformas.component.html',
  styleUrls: ['./plataformas.component.sass']
})
export class PlataformasComponent implements OnInit {

  public plataformas = [];
  public platImgs = ['tablet', 'computer', 'wifi'];

  constructor(private service: AppService, private route: Router) {
    this.showPlataformas();
  }

  ngOnInit() {
  }

  showPlataformas() {
    const app = this;

    this.service.getPlataformas()
      .subscribe((data) => {
        app.plataformas = (data as any).plataformas;
        app.service.plataformas = app.plataformas;
      });
  }

}
